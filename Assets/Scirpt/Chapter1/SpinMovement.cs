using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nopparat.GameDev3.Chapter1
{
    public class SpinMovement : MonoBehaviour
    {
        public float m_AngularSpeed = 5.0f;
        public Vector3 m_Axisofrotation = new Vector3(1.0f, 0, 0);

        private Transform m_ObjTransform;

        // Start is called before the first frame update
        void Start()
        {
            m_ObjTransform = this.gameObject.GetComponent<Transform>();
        }

        // Update is called once per frame
        void Update()
        {
            m_ObjTransform.Rotate(m_Axisofrotation, m_AngularSpeed);
        }
    }
}
