using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nopparat.GameDev3.Chapter4
{


    public enum ItemType
    {
        COIN,
        BIGCOIN,
        POWERUP,
        POWERDOWN,
        medicine,
        poison,
    }

}