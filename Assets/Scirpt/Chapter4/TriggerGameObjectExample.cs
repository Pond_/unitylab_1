using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nopparat.GameDev3.Chapter4
{
    public class TriggerGameObjectExample : MonoBehaviour
    {
        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.tag == "Item")
            {
                Debug.Log("Do something with Item"); 
            }
            if (collider.gameObject.tag == "Player")
            {
                Debug.Log("Do something with Player");
            }
            
        }
    
        // Start is called before the first frame update
        void Start()
        {
            
        }
    
        // Update is called once per frame
        void Update()
        {
            
        }
    }

}
