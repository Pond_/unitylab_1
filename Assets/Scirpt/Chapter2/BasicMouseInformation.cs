using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nopparat.GameDev3.Chapter2
{
  public class BasicMouseInformation : MonoBehaviour
  {
      private Vector3 m_MousePreviousPosition;
      public Text m_TextMousePosition;
      public Text m_TextMouseScorllDelta;
      public Text m_TextMouseDeltaVector;
      // Start is called before the first frame update
      void Start()
      {
          
      }
  
      // Update is called once per frame
      void Update()

      {
          Vector3 mouseCurrenPos = Input.mousePosition;
          Vector3 mouseDeltaVector = Vector3.zero;
          mouseDeltaVector = (mouseCurrenPos - m_MousePreviousPosition).normalized;
          
          m_TextMousePosition.text = Input.mousePosition.ToString();
          m_TextMouseScorllDelta.text = Input.mouseScrollDelta.ToString();
          m_TextMouseDeltaVector.text = mouseDeltaVector.ToString();
          m_MousePreviousPosition = mouseCurrenPos;

      }
  }
  
}
