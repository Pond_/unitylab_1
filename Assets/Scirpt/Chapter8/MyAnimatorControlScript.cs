using UnityEngine;

namespace Nopparat.GameDev3.Chapter8
{

    public class MyAnimatorControlScript : MonoBehaviour
    {
        protected Animator m_Animator;
        private static readonly int Punch = Animator.StringToHash("Punch"); 
        private static readonly int Dancing = Animator.StringToHash("Dancing"); 
        private static readonly int State = Animator.StringToHash("State"); 
        private static readonly int W = Animator.StringToHash("W");
        private static readonly int Turn = Animator.StringToHash("Turn");
        private static readonly int forward = Animator.StringToHash("forward");
        private static readonly int W_T = Animator.StringToHash("W_T"); 
        private static readonly int AT = Animator.StringToHash("AT"); 
        private static readonly int AX = Animator.StringToHash("AX"); 
        private static readonly int AY = Animator.StringToHash("AY"); 

        // Start is called before the first frame update
       private void Start()
        {
            m_Animator = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            /*if (Input.GetKeyDown(KeyCode.Space)){
                m_Animator.SetTrigger(”Jump”);
            }
            if (Input.GetKeyDown(KeyCode.Z)){
                m_Animator.SetTrigger(Punch);
            }
            if (Input.GetKeyDown(KeyCode.X)){
                m_Animator.SetBool(Dancing,true); }*/
                
            if (Input.GetKeyDown(KeyCode.C))
            {
                         
                m_Animator.SetTrigger(W_T);
                
            } 
            if (Input.GetKeyDown(KeyCode.V))
            {
                m_Animator.SetFloat(W,0.25f); 
            }
       
            if (Input.GetKeyDown(KeyCode.B))
            {
                m_Animator.SetFloat(W, 0.5f);
            }

            if (Input.GetKeyDown(KeyCode.N))
            {
                m_Animator.SetFloat(W, 0.75f);
            } 
            if (Input.GetKeyDown(KeyCode.M))
            { 
                m_Animator.SetFloat(W, 1);
                             
            }

            /* if (Input.GetKeyDown(KeyCode.V))
             {
                 m_Animator.SetFloat(W,0.5f); 
             }
             if (Input.GetKeyDown(KeyCode.B))
             {
                 m_Animator.SetFloat(W,0.75f); 
             }*/
          
            
            if (Input.GetKeyDown(KeyCode.W))
            {
                m_Animator.SetFloat(forward, 0.5f);
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                m_Animator.SetFloat(Turn, 0.5f);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                m_Animator.SetFloat(Turn, -0.5f);
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                m_Animator.SetTrigger(AT);
            } 
            if (Input.GetKeyDown(KeyCode.R))
            {
                m_Animator.SetFloat(AX, 0);
                m_Animator.SetFloat(AY, 0);
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                m_Animator.SetFloat(AX, 0.5f);
                m_Animator.SetFloat(AY, 0);
            }
            if (Input.GetKeyDown(KeyCode.Y))
            {
                m_Animator.SetFloat(AX, 0);
                m_Animator.SetFloat(AY, 0.5f);
            }
            if (Input.GetKeyDown(KeyCode.U))
            {
                m_Animator.SetFloat(AX, -0.5f);
                m_Animator.SetFloat(AY, 0);
            }
            if (Input.GetKeyDown(KeyCode.I))
            {
                m_Animator.SetFloat(AX, 0);
                m_Animator.SetFloat(AY, -0.5f);
            }
            if (Input.GetKeyDown(KeyCode.O))
            {
                m_Animator.SetFloat(AX, 0.5f);
                m_Animator.SetFloat(AY, 0.5f);
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                m_Animator.SetFloat(AX, -0.5f);
                m_Animator.SetFloat(AY, -0.5f);
            }
            
        }
    }
}
